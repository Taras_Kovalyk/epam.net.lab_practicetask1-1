﻿using System;

namespace EpamITLab.Task4
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Creating new matrix 1");
            var matr1 = new Matrix(new double[,] { { 1, -2, 4 }, { 2, 0, -1 }, { 3, -5, 4 } });
            Console.WriteLine("Matrix 1 contains:");
            Console.WriteLine(matr1);

            Console.WriteLine("Creating new matrix 2");
            var matr2 = new Matrix(new double[,] { { -3, 0, 2 }, { 2, 3, -2 }, { 1, 0, 3 } });
            Console.WriteLine("Matrix 2 contains:");
            Console.WriteLine(matr2);

            Console.WriteLine("Calculation sum of matrices");
            var sum = Matrix.Add(matr1, matr2);
            Console.WriteLine("Sum of matrices:");
            Console.WriteLine(sum);

            Console.WriteLine("Calculation subtraction of matrices");
            var subtr = matr1 - matr2;
            Console.WriteLine("Subtraction of matrices:");
            Console.WriteLine(subtr);

            Console.WriteLine("Calculation multiplication of matrices");
            var mult = matr1.Multiply(matr2);
            Console.WriteLine("Multiplication of matrices:");
            Console.WriteLine(mult);

            Console.WriteLine("Calculation 1-th minor of matrix 1");
            Console.WriteLine($"1-th minor of matrix 1 = {matr1.KthMinor(1)}");

            Console.WriteLine("Calculation of 0 row and 2-nd column minor of matrix 2");
            Console.WriteLine($"0 row and 2-nd column minor of matrix 2 = {Matrix.RowColumnMinor(matr2, 0, 2)}");

            Console.ReadLine();
        }
    }
}
