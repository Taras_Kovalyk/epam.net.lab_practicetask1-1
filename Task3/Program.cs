﻿using System;

namespace EpamITLab.Task3
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Creating new polynome 1 of power 3");
            var pol1 = new Polynome(3, new double[] {4, -2, 3, 5});
            Console.WriteLine("Polynome 1 is:");
            Console.WriteLine(pol1);

            Console.WriteLine("Creating new polynome 2 of power 4");
            var pol2 = new Polynome(4, -1, 5, 3, -2, 8);
            Console.WriteLine("Polynome 2 is:");
            Console.WriteLine(pol2);

            const int arg1 = 4;
            const int arg2 = -3;

            Console.WriteLine($"Polynome 1 value if x = {arg1} = {pol1.CalculateValue(arg1)}");
            Console.WriteLine($"Polynome 2 value if x = {arg2} = {pol2.CalculateValue(arg2)}");

            Console.WriteLine("Calculate sum of polynome 1 and polynome 2");
            var sum = pol1 + pol2;
            Console.WriteLine($"Sum of polynoms = {sum}");

            Console.WriteLine("Calculate subtraction of polynome 1 and polynoe 2");
            var subtraction = Polynome.Subtract(pol1, pol2);
            Console.WriteLine($"Subtraction of polynoms = {subtraction}");

            Console.WriteLine("Calculate multiplication of polynome 1 and polynome 2");
            var mult = pol1.Multiply(pol2);
            Console.WriteLine($"Multiplication of polynoms = {mult}");

            Console.WriteLine($"Multiplication value if x = {arg1} = {mult.CalculateValue(arg1)}");

            Console.ReadKey();
        }
    }
}
