﻿using System;
using System.Linq;

namespace EpamITLab.Task1
{
    public sealed class Vector
    {
        private int[] _array;
        private const int DefaultLength = 4;
        private int _leftLimit;
        private int _rightLimit;
        public int Length => _array.Length;
        public int Count => _rightLimit - _leftLimit + 1;

        public int this[int index]
        {
            get
            {
                if (index < _leftLimit || index > _rightLimit)
                {
                    throw new ArgumentOutOfRangeException(nameof(index), index, "Index was out of range");
                }

                return _array[index];
            }
            set
            {
                if (index < _leftLimit || index > _rightLimit)
                {
                    throw new ArgumentOutOfRangeException(nameof(index), index, "Index was out of range");
                }

                _array[index] = value;
            }
        }

        public Vector()
        {
            _array = new int[DefaultLength];
            _leftLimit = 0;
            _rightLimit = DefaultLength - 1;
        }

        public Vector(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null");
            }

            _array = array;
            _leftLimit = 0;
            _rightLimit = array.Length - 1;
        }

        public Vector(int[] array, int startIndex, int length) : this(array)
        {
            if (array.Length > length - startIndex || startIndex < 0 || length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), length, "Array length was out of vector size");
            }

            _array = new int[startIndex + length];

            for (int i = startIndex, j = 0; j < array.Length; i++, j++)
            {
                _array[i] = array[j];
            }

            _leftLimit = startIndex;
            _rightLimit = startIndex + length;
        }

        public Vector(Vector vector)
        {
            if (vector == null)
            {
                throw new ArgumentNullException(nameof(vector), "Vector cannot be null");
            }

            _array = vector._array;
            _leftLimit = vector._leftLimit;
            _rightLimit = vector._rightLimit;
        }

        public Vector(Vector vector, int startIndex, int length) : this(vector)
        {
            if (vector.Count > length - startIndex || startIndex < 0 || length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), length, "Array length was out of vector size");
            }

            _array = new int[startIndex + length];

            for (int i = startIndex, j = vector._leftLimit; j <= vector._rightLimit; i++, j++)
            {
                _array[i] = vector._array[j];
            }

            _leftLimit = startIndex;
            _rightLimit = startIndex + length;
        }

        public static Vector operator +(Vector lhs, Vector rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Vector cannot be null");
            }

            if (lhs.Count != rhs.Count)
            {
                throw new ArgumentException("Cannot add vectors with different counts of elements");
            }

            var temp = new int[lhs.Length];

            for (int i = lhs._leftLimit; i <= lhs._rightLimit; i++)
            {
                temp[i] = lhs[i] + rhs[i];
            }

            return new Vector { _array = temp, _leftLimit = lhs._leftLimit, _rightLimit = lhs._rightLimit };
        }

        public static Vector operator -(Vector lhs, Vector rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Vector cannot be null");
            }

            if (lhs.Count != rhs.Count)
            {
                throw new ArgumentException("Cannot subtract vectors with different counts of elements");
            }

            var temp = new int[lhs.Length];

            for (int i = lhs._leftLimit; i <= lhs._rightLimit; i++)
            {
                temp[i] = lhs[i] - rhs[i];
            }

            return new Vector { _array = temp, _leftLimit = lhs._leftLimit, _rightLimit = lhs._rightLimit };
        }

        public static Vector operator *(Vector lhs, int scalar)
        {
            if (lhs == null)
            {
                throw new ArgumentNullException(nameof(lhs), "Vector cannot be null");
            }

            var temp = new int[lhs.Length];

            for (int i = 0; i < lhs.Length; i++)
            {
                temp[i] = lhs[i] * scalar;
            }

            return new Vector { _array = temp, _leftLimit = lhs._leftLimit, _rightLimit = lhs._rightLimit };
        }

        public static bool operator ==(Vector lhs, Vector rhs)
        {
            if (lhs?.Count != rhs?.Count || lhs?.Length != rhs?.Length)
            {
                return false;
            }

            if (lhs?._leftLimit != rhs?._leftLimit || lhs?._rightLimit != rhs?._rightLimit)
            {
                return false;
            }

            for (var i = lhs?._leftLimit; i < lhs?._rightLimit; i++)
            {
                if (lhs[(int) i] != rhs[(int) i])
                {
                    return false;
                }
            }

            return true;
        }

        public static bool operator !=(Vector lhs, Vector rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object obj)
        {
            var elementToCompare = obj as Vector;

            if (elementToCompare == null)
            {
                throw new InvalidCastException("Cannot implicitly cast object to Vector");
            }

            return this == elementToCompare;
        }

        public override int GetHashCode()
        {
            // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
        }

        public override string ToString()
        {
            var result = _array.Aggregate("(", (current, coordinate) => current + (coordinate + "  "));
            result += $"); Count = {Count}";

            return result;
        }
    }
}
