﻿using System;

namespace EpamITLab.Task2
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Creating new rectangle 1");
            var rectangle1 = new Rectangle(0, 0, 200, 100);
            Console.WriteLine("Coordinates of rectangle1:");
            Console.WriteLine(rectangle1);

            Console.WriteLine("Move rectagnle 1 to new point (100, 100)");
            rectangle1.MoveTo(100, 100);
            Console.WriteLine("Coordinates of rectangle1:");
            Console.WriteLine(rectangle1);

            Console.WriteLine("Change width of rectangle 1 to 300");
            rectangle1.Width = 300;
            Console.WriteLine("Coordinates of rectangle1:");
            Console.WriteLine(rectangle1);

            Console.WriteLine("Change height of rectangle 1 to 150");
            rectangle1.Height = 150;
            Console.WriteLine("Coordinates of rectangle1:");
            Console.WriteLine(rectangle1);

            Console.WriteLine("Creating new rectangle 2");
            var rectangle2 = new Rectangle(50, 100, 100, 100);
            Console.WriteLine("Coordinates of rectangle2:");
            Console.WriteLine(rectangle2);

            Console.WriteLine("Create rectangle 3 that is intersection of rectangle 1 and rectangle 2");
            var intersect = Rectangle.Intersect(rectangle1, rectangle2);
            Console.WriteLine("Coordinates of rectangle 3");
            Console.WriteLine(intersect);

            Console.WriteLine("Create rectangle 4 that is union of rectangle 1 and rectangle 2");
            var union = Rectangle.Union(rectangle1, rectangle2);
            Console.WriteLine("Coordinates of rectangle 4");
            Console.WriteLine(union);

            Console.ReadLine();
        }
    }
}
